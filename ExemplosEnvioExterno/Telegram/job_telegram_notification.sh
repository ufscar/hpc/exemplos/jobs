#!/bin/bash
#SBATCH --job-name=TelegramTest
#SBATCH --output=Output.o
#SBATCH --error=Error.o

# Environment config
localJob="/scratch/job.${SLURM_JOB_ID}"
mkdir $localJob

pythonProgram="Agrupamento.py"
datasetPreview="${localJob}/dataset.png"
datasetResult="${localJob}/resultado.png"
experimentResult="${localJob}/resultado.csv"

# Telegram variables
chatId=""
botKey=""

# Você pode encontrar essa imagem em: 
pythonSimg=""


# Recomendo copiar estas funções para jobs que irão utilizar essa comunicação
function sendPhoto(){
  curl -F "chat_id=$chatId" -F "photo=@$1" "https://api.telegram.org/$botKey/sendphoto"
}

function sendFile(){
  curl -F "chat_id=$chatId" -F "document=@$1" "https://api.telegram.org/$botKey/sendDocument"
}

function sendMsg(){
  curl -X POST -H 'Content-Type: application/json' -d "{\"chat_id\": \"$chatId\", \"text\": \"$1\", \"disable_notification\": true}"  "https://api.telegram.org/$botKey/sendMessage"
}

function cleanJob(){
  echo "Limpando ambiente..."
  rm -rf "${localJob}"
}
trap cleanJob EXIT HUP INT TERM

function sendErr(){
  sendMsg "Exited with error!"
  sendFile "Error.e"
  sendFile "Output.o"
  cleanJob
}
trap ERR 

sendMsg "Iniciando execução de ${SLURM_JOB_ID}"

srun singularity run \
	--bind=/scratch:/scratch \
	--bind=/var/spool/slurm:/var/spool/slurm \
	$pythonSimg $pythonProgram $datasetPreview $datasetResult $experimentResult 

retCode=$?
if [[ "$retCode" -ne 0 ]]; then
  sendErr 
  exit 1
fi

sendPhoto $datasetPreview
sendMsg "Job ${SLURM_JOB_ID} finalizado com sucesso!"
sendPhoto $datasetResult 
sendFile $experimentResult

