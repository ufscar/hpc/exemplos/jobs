#!/bin/bash
#SBATCH --job-name=TelegramTest
#SBATCH --output=Output.o
#SBATCH --error=Error.o

# Environment config
localJob="/scratch/job.${SLURM_JOB_ID}"
mkdir $localJob

pythonProgram="Agrupamento.py"
datasetPreview="${localJob}/dataset.png"
datasetResult="${localJob}/resultado.png"
experimentResult="${localJob}/resultado.csv"

discordBotKey=""

# Você pode encontrar essa imagem em: https://gitlab.com/ufscar/hpc/jupyter-datascience
pythonSimg=""

function sendFile(){
	curl -i -H 'Expect: application/json' -F file=@$1 -F 'payload_json={ "wait": true}' $discordBotKey 
}

function sendMsg(){
	curl $discordBotKey -F {"message":"$1"}
}

function cleanJob(){
  echo "Limpando ambiente..."
  rm -rf "${localJob}"
}
trap cleanJob EXIT HUP INT TERM

function sendErr(){
  sendMsg "Exited with error!"
  sendFile "Error.e"
  sendFile "Output.o"
  cleanJob
}
trap ERR 

sendMsg "Iniciando execução de ${SLURM_JOB_ID}"

srun singularity run \
	--bind=/scratch:/scratch \
	--bind=/var/spool/slurm:/var/spool/slurm \
	$pythonSimg $pythonProgram $datasetPreview $datasetResult $experimentResult 

retCode=$?
if [[ "$retCode" -ne 0 ]]; then
  sendErr 
  exit 1
fi

sendFile $datasetPreview
sendMsg "Job ${SLURM_JOB_ID} finalizado com sucesso!"
sendFile $datasetResult 
sendFile $experimentResult

