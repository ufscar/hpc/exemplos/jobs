# Exemplos para novo Usuário

Exemplos para novos usuários de arquivos a serem usados tanto no cluster como no computador local

- `job_singularity_ompi.sh` - Job usando container singularity e compilador OpenMPI
- `job_singularity_mpich.sh` - Job usando container singularity e compilador MPICH
- `job_singularity_gpu.sh` - Job usando container singularity e para uso na GPU
- `ExemploEnvioExterno` - Exemplo de como enviar dados de execução dos jobs via Telegram e Discord